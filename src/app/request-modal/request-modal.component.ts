import { ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { AbstractControl, FormArray, FormBuilder, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import * as moment from 'moment';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { ReCaptcha2Component } from 'ngx-captcha';
import { GooglePlaceDirective } from 'ngx-google-places-autocomplete';
import { Address } from 'ngx-google-places-autocomplete/objects/address';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { SubSink } from 'subsink';
import { HomeService } from '../home.service';
import { LocationService } from '../location.service';
interface AddressComponents {
	long_name: string;
	short_name: string;
	types: string[];
}
@Component({
	selector: 'app-request-modal',
	templateUrl: './request-modal.component.html',
	styleUrls: ['./request-modal.component.css'],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class RequestModalComponent implements OnInit, OnDestroy {
	requestForm: FormGroup;
	subs = new SubSink();
	minTime: Date;
	min: Date;
	startAt: Date;
	siteKey = '6Lf9ZuQZAAAAAC4qh2Wzdhfb_jr9tuWxip39U8WO';
	size = 'Normal';
	theme = 'Light';
	type = 'Image';
	lang = 'en';
	options = {
		types: [],
		componentRestrictions: { country: 'USA' }
	};
	@ViewChild('placesRef') placesRef: GooglePlaceDirective;
	@ViewChild('dateInput', { static: true }) dateTyped: ElementRef;
	@ViewChild('captchaElem', { static: false }) captchaElem: ReCaptcha2Component;
	constructor(
		private bsModalRef: BsModalRef,
		private fb: FormBuilder,
		private locService: LocationService,
		private toastr: ToastrService,
		private cd: ChangeDetectorRef,
		private service: HomeService,
		private spinner: NgxSpinnerService
	) {
		this.requestForm = this.fb.group({
			meetingName: [null, Validators.compose([Validators.maxLength(40), Validators.pattern('^[a-zA-Z \-\']+')])],
			meetingEmail: [null, Validators.compose([Validators.pattern(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)])],
			meetingMobile: [null, Validators.compose([this.customValidatorUSnumber])],
			meetingFax: [null, Validators.compose([this.customValidatorUSFAXnumber])],
			meetingDate: [''],
			meetingTime: [''],
			meetingAddress: [''],
			houseNoFlatNo: [''],
			addressLandmark: [''],
			meetingZip: [null, Validators.compose([this.customValidatorZip])],
			meetingCity: [''],
			meetingState: [''],
			recaptcha: [''],
		});
		// for disabling previous dates from current date
		let month: any;
		let day: any;
		const dtToday = new Date();
		const year = dtToday.getFullYear();
		let hour = dtToday.getHours();
		const minutes = dtToday.getMinutes();
		const seconds = dtToday.getSeconds();
		month = dtToday.getMonth() + 1;
		day = dtToday.getDate();
		if (month < 10) {
			month = '0' + month.toString();
		}
		if (day < 10) {
			day = '0' + day.toString();
		}
		if (minutes >= 45) {
			hour++;
		}
		this.min = new Date(year, month - 1, day);
		this.startAt = new Date
			(
				year, month - 1, day, hour,
				minutes >= 0 && minutes < 15 ? 15 :
					minutes >= 15 && minutes < 30 ? 30 :
						minutes >= 30 && minutes < 45 ? 45 :
							minutes >= 45 ? 0 : 15, seconds
			);
	}
	ngOnDestroy(): void {
		this.subs.unsubscribe();
	}
	ngOnInit(): void {
		this.subs.add(this.locService.get().subscribe((res: { add: { components: AddressComponents[], formatted_address: string }; position_latitude: number; position_longitude: number; }) => {
			if (res.add.components.length > 0) {
				const formatted = res.add.formatted_address.split(', ');
				this.requestForm.get('meetingAddress').patchValue(formatted.length >= 2 ? `${formatted[0]}, ${formatted[1]}` : `${formatted[0]}`);
				this.requestForm.get('addressLandmark').patchValue(res.add.components.filter(c => c.types.find(t => t === 'sublocality'))[0]?.long_name ? res.add.components.filter(c => c.types.find(t => t === 'sublocality'))[0].long_name : '');
				this.requestForm.get('meetingZip').patchValue(res.add.components.filter(c => c.types.filter(t => t === 'postal_code')[0])[0]?.long_name ? res.add.components.filter(c => c.types.filter(t => t === 'postal_code')[0])[0].long_name : '0');
				this.requestForm.get('meetingCity').patchValue(res.add.components.filter(c => c.types.find(t => t === 'locality'))[0]?.long_name ? res.add.components.filter(c => c.types.find(t => t === 'locality'))[0].long_name : '');
				this.requestForm.get('meetingState').patchValue(res.add.components.filter(c => c.types.find(t => t === 'administrative_area_level_1'))[0]?.long_name ? res.add.components.filter(c => c.types.find(t => t === 'administrative_area_level_1'))[0].long_name : '');
				this.cd.markForCheck();
			}
		}, err => console.error(err)));
		this.subs.add(this.requestForm.get('meetingTime').valueChanges.pipe().subscribe(() => { this.check(); }));
		this.subs.add(this.requestForm.get('meetingDate').valueChanges.pipe().subscribe(() => { this.timeAndDate(); }));
	}
	customValidatorZip(control: AbstractControl): ValidationErrors {
		const error = {
			name: '',
			message: ''
		};
		const zipRegex = /^((\\+91-?)|0)?[0-9]{5}$/;
		const patternValidZip = /^(?!0+$)\d{5,}$/;
		if (control.value) {
			if (!zipRegex.test(control.value)) {
				error.name = 'invalidZip';
				error.message = 'zip code must be 5 digits.';
				return error;
			}
			if (!patternValidZip.test(control.value)) {
				error.name = 'invalidZip';
				error.message = 'Please enter valid zip code.';
				return error;
			}
			return null;
		}
		return null;
	}
	customValidatorUSnumber(control: AbstractControl): ValidationErrors {
		const error = {
			name: '',
			message: ''
		};
		const phoneRegex = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
		const patternValidPhone = /^(?!(\d)\1{9})(?!0123456789|1234567890|0987654321|9876543210)\d{10}$/;
		if (control.value) {
			if (!phoneRegex.test(control.value)) {
				error.name = 'invalidPhone';
				error.message = 'Phone number must be only 10 digits.';
				return error;
			}
			if (!patternValidPhone.test(control.value)) {
				error.name = 'invalidPhone';
				error.message = 'Please enter valid phone number.';
				return error;
			}
			return null;
		}
		return null;
	}
	customValidatorUSFAXnumber(control: AbstractControl): ValidationErrors {
		const error = {
			name: '',
			message: ''
		};
		const phoneRegex = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
		const patternValidPhone = /^(?!(\d)\1{9})(?!0123456789|1234567890|0987654321|9876543210)\d{10}$/;
		if (control.value) {
			if (!phoneRegex.test(control.value)) {
				control.value.replace(phoneRegex, '($1) $2-$3');
				error.name = 'invalidPhone';
				error.message = 'Fax number must be only 10 digit.';
				return error;
			}
			if (!patternValidPhone.test(control.value)) {
				error.name = 'invalidPhone';
				error.message = 'Please enter valid fax number.';
				return error;
			}
			return null;
		}
		return null;
	}
	check = () => {
		if (this.requestForm.get('meetingDate').value === '') {
			this.requestForm.get('meetingTime').patchValue('', { emitEvent: false });
			this.requestForm.get('meetingTime').setErrors({ emptyDate: true });
			this.cd.markForCheck();
		} else {
			this.requestForm.get('meetingTime').setErrors(null);
			this.requestForm.get('meetingTime').updateValueAndValidity({ emitEvent: false });
			this.cd.markForCheck();
		}
	}
	afterPickerOpen = () => {
		document.getElementsByClassName('owl-dt-timer-input')?.item(0).setAttribute('readOnly', 'true');
		document.getElementsByClassName('owl-dt-timer-input')?.item(1).setAttribute('readOnly', 'true');
	}
	onClose = () => {
		this.bsModalRef.hide();
	}
	timeAndDate = () => {
		const date1 = new Date();
		let month: any;
		let day: any;
		const dtToday = new Date();
		const year = dtToday.getFullYear();
		let hour = dtToday.getHours();
		month = dtToday.getMonth() + 1;
		day = dtToday.getDate();
		const minutes = dtToday.getMinutes();
		const seconds = dtToday.getSeconds();
		if (month < 10) {
			month = '0' + month.toString();
		}
		if (day < 10) {
			day = '0' + day.toString();
		}
		if (minutes >= 45) {
			hour++;
		}
		const date2 = new Date(this.requestForm.get('meetingDate').value);
		date2.setHours(hour, minutes, 0, 0);
		date1.setHours(hour, minutes, 0, 0);
		if (date1.getTime() === date2.getTime()) {
			this.minTime = new Date();
			this.cd.markForCheck();
		} else {
			this.minTime = null;
			this.cd.markForCheck();
		}
		this.startAt = new Date
			(
				year, month - 1, day, hour,
				minutes >= 0 && minutes < 15 ? 15 :
					minutes >= 15 && minutes < 30 ? 30 :
						minutes >= 30 && minutes < 45 ? 45 :
							minutes >= 45 ? 0 : 15, seconds
			);
		this.requestForm.get('meetingTime').patchValue('', { emitEvent: false });
		this.requestForm.get('meetingTime').updateValueAndValidity({ emitEvent: false });
		this.cd.markForCheck();
	}

	onSubmit = (post: {
		meetingName: string;
		meetingEmail: string;
		meetingMobile: string;
		meetingFax: string;
		meetingDate: string;
		meetingTime: string;
		meetingAddress: string;
		houseNoFlatNo: string;
		addressLandmark: string;
		meetingZip: string;
		meetingCity: string;
		meetingState: string;
		recaptcha: string;
	}) => {
		this.markFormTouched(this.requestForm);
		if (!this.checkControlPost(post)) {
			if (this.requestForm.valid && this.findInvalidControls().length === 0) {
				const data = {
					meetingName: post.meetingName ? post.meetingName.trim() : '',
					meetingEmail: post.meetingEmail ? post.meetingEmail.trim() : '',
					meetingMobile: post.meetingMobile ? post.meetingMobile.match(/\d/g).join('').trim() : '',
					meetingFax: post.meetingFax ? post.meetingFax.match(/\d/g).join('').trim() : '',
					meetingDate: post.meetingDate && post.meetingTime ?
						`${moment(post.meetingDate).format('YYYY-MM-DD')} ${moment(post.meetingTime, 'h:mm:ss A').format('HH:mm:ss')}` : '',
					meetingLocation: post.meetingAddress && post.meetingZip && post.meetingCity && post.meetingState ?
						`${post.meetingAddress.trim()}, ${post.houseNoFlatNo.trim()}, ${post.addressLandmark.trim()}, ${post.meetingZip.trim()}, ${post.meetingCity.trim()}, ${post.meetingState.trim()}` : '',
				};
				this.request(JSON.stringify(data)).then(() => {
					setTimeout(() => {
						this.openToastr(post.meetingDate, post.meetingTime);
						setTimeout(() => {
							this.requestForm.reset();
							this.bsModalRef.hide();
						}, 500);
					}, 1000);
				}).catch(() => this.toastr.error('Error', 'Opps! something went wrong, please try again later.'))
					.finally(() => this.spinner.hide());
			}
		}
		if (this.checkControlPost(post)) {
			this.markFormTouched(this.requestForm);
		}
	}
	checkControlPost = (post: {
		meetingName: string;
		meetingEmail: string;
		meetingMobile: string;
		meetingFax: string;
		meetingDate: string;
		meetingTime: string;
		meetingAddress: string;
		houseNoFlatNo: string;
		addressLandmark: string;
		meetingZip: string;
		meetingCity: string;
		meetingState: string;
		recaptcha: string;
	}) => {
		let invalid = false;
		Object.keys(post).forEach((key: string) => {
			if (key === 'meetingName' && !this.requestForm.get(`${key}`).value) {
				this.requestForm.get(`${key}`).setValidators([Validators.required, Validators.maxLength(40), Validators.pattern('^[a-zA-Z \-\']+')]);
				this.requestForm.get(`${key}`).updateValueAndValidity({ onlySelf: true });
				return invalid = true;
			}
			if (key === 'meetingEmail' && !this.requestForm.get(`${key}`).value) {
				this.requestForm.get(`${key}`).setValidators([Validators.required, Validators.pattern(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)]);
				this.requestForm.get(`${key}`).updateValueAndValidity({ onlySelf: true });
				return invalid = true;
			}
			if (key === 'meetingMobile' && !this.requestForm.get(`${key}`).value) {
				this.requestForm.get(`${key}`).setValidators([Validators.required, this.customValidatorUSnumber]);
				this.requestForm.get(`${key}`).updateValueAndValidity({ onlySelf: true });
				return invalid = true;
			}
			if (key === 'meetingDate' && !this.requestForm.get(`${key}`).value) {
				this.requestForm.get(`${key}`).setValidators([Validators.required]);
				this.requestForm.get(`${key}`).updateValueAndValidity({ onlySelf: true, emitEvent: false });
				return invalid = true;
			}
			if (key === 'meetingTime' && !this.requestForm.get(`${key}`).value) {
				this.requestForm.get(`${key}`).setValidators([Validators.required]);
				this.requestForm.get(`${key}`).updateValueAndValidity({ onlySelf: true, emitEvent: false });
				return invalid = true;
			}
			if (key === 'meetingAddress' && !this.requestForm.get(`${key}`).value) {
				this.requestForm.get(`${key}`).setValidators([Validators.required]);
				this.requestForm.get(`${key}`).updateValueAndValidity({ onlySelf: true });
				return invalid = true;
			}
			if (key === 'houseNoFlatNo' && !this.requestForm.get(`${key}`).value) {
				this.requestForm.get(`${key}`).setValidators([Validators.required]);
				this.requestForm.get(`${key}`).updateValueAndValidity({ onlySelf: true });
				return invalid = true;
			}
			if (key === 'addressLandmark' && !this.requestForm.get(`${key}`).value) {
				this.requestForm.get(`${key}`).setValidators([Validators.required]);
				this.requestForm.get(`${key}`).updateValueAndValidity({ onlySelf: true });
				return invalid = true;
			}
			if (key === 'meetingZip' && !this.requestForm.get(`${key}`).value) {
				this.requestForm.get(`${key}`).setValidators([Validators.required, this.customValidatorZip]);
				this.requestForm.get(`${key}`).updateValueAndValidity({ onlySelf: true });
				return invalid = true;
			}
			if (key === 'meetingCity' && !this.requestForm.get(`${key}`).value) {
				this.requestForm.get(`${key}`).setValidators([Validators.required]);
				this.requestForm.get(`${key}`).updateValueAndValidity({ onlySelf: true });
				return invalid = true;
			}
			if (key === 'meetingState' && !this.requestForm.get(`${key}`).value) {
				this.requestForm.get(`${key}`).setValidators([Validators.required]);
				this.requestForm.get(`${key}`).updateValueAndValidity({ onlySelf: true });
				return invalid = true;
			}
			if (key === 'recaptcha' && !this.requestForm.get(`${key}`).value) {
				this.requestForm.get(`${key}`).setValidators([Validators.required]);
				this.requestForm.get(`${key}`).updateValueAndValidity({ onlySelf: true });
				return invalid = true;
			}
		});
		return invalid;
	}
	request = (post: string) => {
		this.spinner.show();
		return new Promise((resolve, reject) => {
			this.subs.add(this.service.requestMeeting(post).subscribe(
				(res) => {
					if (res[0].status === 'true') {
						resolve(res[0].message);
					} else {
						reject(res[0].message);
					}
				}, () => reject('error')));
		});
	}
	findInvalidControls = () => {
		const invalid = [];
		const controls = this.requestForm.controls;
		for (const name in controls) {
			if (controls[name].invalid) {
				invalid.push(name);
			}
		}
		return invalid;
	}
	markFormTouched = (group: FormGroup | FormArray) => {
		Object.keys(group.controls).forEach((key: string) => {
			const control = group.controls[key];
			if (control instanceof FormGroup || control instanceof FormArray) {
				control.markAsTouched();
				this.markFormTouched(control);
			} else {
				control.markAsTouched();
			}
		});
	}
	openToastr = (date: string, time: string) => {
		// this.toastr.success(`<h5>Your Appointment is confirmed on ${moment(date).format('dddd, MMMM Do YYYY')} at ${moment(time, 'h:mm:ss A').format('hh:mm a')}.</h5>You will get notifications with details.`
		// 	, '', {
		// 	positionClass: 'toast-center-center-request',
		// 	closeButton: true,
		// 	timeOut: 9000,
		// 	enableHtml: true,
		// });
		this.toastr.success(`<h5>Thank you for your interest.</h5>Our team will reach out to you.`
			, '', {
			positionClass: 'toast-center-center',
			closeButton: false,
			timeOut: 9000,
			enableHtml: true,
		});
	}
	handleAddressChange = (address: Address) => {
		this.subs.add(this.locService.getLocationFromLatLng(address.geometry.location.lat(), address.geometry.location.lng()).subscribe(
			(add: { components: AddressComponents[], formatted_address: string }) => {
				if (add.components.length > 0) {
					const formatted = add.formatted_address.split(', ');
					this.requestForm.get('meetingAddress').patchValue(formatted.length >= 2 ? `${formatted[0]}, ${formatted[1]}` : `${formatted[0]}`);
					this.requestForm.get('addressLandmark').patchValue(add.components.filter(c => c.types.find(t => t === 'sublocality'))[0]?.long_name ? add.components.filter(c => c.types.find(t => t === 'sublocality'))[0].long_name : '');
					this.requestForm.get('meetingZip').patchValue(add.components.filter(c => c.types.filter(t => t === 'postal_code')[0])[0]?.long_name ? add.components.filter(c => c.types.filter(t => t === 'postal_code')[0])[0].long_name : '0');
					this.requestForm.get('meetingCity').patchValue(add.components.filter(c => c.types.find(t => t === 'locality'))[0]?.long_name ? add.components.filter(c => c.types.find(t => t === 'locality'))[0].long_name : '');
					this.requestForm.get('meetingState').patchValue(add.components.filter(c => c.types.find(t => t === 'administrative_area_level_1'))[0]?.long_name ? add.components.filter(c => c.types.find(t => t === 'administrative_area_level_1'))[0].long_name : '');
					this.cd.markForCheck();
				}
			}, err => console.error(err)));
	}
	onKeyDate = (value: string) => {
		let input = value;
		if (/\D\/$/.test(input)) { input = input.substr(0, input.length - 3); }
		const values = input.split('/').map((v) => v.replace(/\D/g, ''));
		if (values[0]) { values[0] = this.checkValue(values[0], 12); }
		if (values[1]) { values[1] = this.checkValue(values[1], 31); }
		const output = values.map((v, i) => v.length === 2 && i < 2 ? v + ' / ' : v);
		this.dateTyped.nativeElement.value = output.join('').substr(0, 14);
	}
	checkValue = (str: string, max: number) => {
		if (str.charAt(0) !== '0' || str === '00') {
			let num = +str;
			if (isNaN(num) || num <= 0 || num > max) { num = 1; }
			str = num > +(max.toString().charAt(0)) && num.toString().length === 1 ? '0' + num : num.toString();
		}
		return str;
	}
	onBlur = (value: string) => {
		const input = value;
		const values = input.split('/').map((v) => v.replace(/\D/g, ''));
		let output = '';
		if (values.length === 3) {
			const year = values[2].length !== 4 ? +(values[2]) + 2000 : +(values[2]);
			const month = +(values[0]) - 1;
			const day = +(values[1]);
			const d = new Date(year, month, day);
			if (!isNaN(+d)) {
				document.getElementById('date').innerText = d.toString();
				const dates = [d.getMonth() + 1, d.getDate(), d.getFullYear()];
				output = dates.map((v) => {
					v = v;
					return v.toString().length === 1 ? '0' + v : v;
				}).join(' / ');
			}
		}
		if (output) {
			this.dateTyped.nativeElement.value = output.replace(/\s/g, '');
			this.requestForm.get('meetingDate').setValue(new Date(+output.split('/')[2], +output.split('/')[0] - 1, +output.split('/')[1]), { emitEvent: false });
			this.requestForm.get('meetingDate').updateValueAndValidity({ emitEvent: false });
			this.cd.markForCheck();
		}
		if (!output) {
			this.dateTyped.nativeElement.value = '';
			this.requestForm.get('meetingDate').patchValue('', { emitEvent: false });
			this.requestForm.get('meetingDate').updateValueAndValidity({ emitEvent: false });
			this.cd.markForCheck();
		}
	}
	handleReset = () => {
		this.requestForm.get('recaptcha').clearValidators();
		this.requestForm.get('recaptcha').updateValueAndValidity();
		this.requestForm.get('recaptcha').patchValue(null);
	}
	handleExpire = () => {
		this.requestForm.get('recaptcha').patchValue(null);
	}
	handleLoad = () => {
		this.requestForm.get('recaptcha').patchValue(null);
	}
	handleSuccess = ($event: any) => {
		this.requestForm.get('recaptcha').patchValue($event);
	}
}
