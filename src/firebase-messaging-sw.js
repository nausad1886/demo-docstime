importScripts("https://www.gstatic.com/firebasejs/8.8.0/firebase-app.js");
importScripts("https://www.gstatic.com/firebasejs/8.8.0/firebase-messaging.js");

firebase.initializeApp({
	apiKey: "AIzaSyDhEd1QHsJM9m8yKI4AtoErPeqMKmOJG4s",
	authDomain: "menukard-66b1e.firebaseapp.com",
	projectId: "menukard-66b1e",
	storageBucket: "menukard-66b1e.appspot.com",
	messagingSenderId: "964534995904",
	appId: "1:964534995904:web:3c0a90c26b69ed9a7ad625",
	measurementId: "G-86Q6H4TXDJ",
});
const messaging = firebase.messaging();
messaging.setBackgroundMessageHandler(function (payload) {
	const promiseChain = clients.matchAll({
		type: 'window',
		includeUncontrolled: true
	}).then((windowClients) => {
		for (let i = 0; i < windowClients.length; i++) {
			const windowClient = windowClients[i];
			windowClient.postMessage(payload);
		}
	}).then(() => {
		const notificationTitle = payload.data.title;
		const notificationOptions = {
			body: payload.data.body,
			icon: 'assets/images/push.png',
			sound: 'default'
		};
		return self.registration.showNotification(notificationTitle, notificationOptions);
	});
	return promiseChain;
});
self.addEventListener('notificationclick', function (event) {
	const urlToOpenNew = new URL('/r/orders/new', self.location.origin).href;
	const urlToOpenOngoing = new URL('/r/orders/ongoing', self.location.origin).href;
	const urlToOpenCompleted = new URL('/r/orders/completed', self.location.origin).href;
	const urlToOpenMenu = new URL('/r/menu', self.location.origin).href;
	const urlToOpenProfile = new URL('/r/profile', self.location.origin).href;
	const urlToOpenReport = new URL('/r/report', self.location.origin).href;
	const promiseChain = clients.matchAll({
		type: 'window',
		includeUncontrolled: true
	}).then((windowClients) => {
		let matchingClient = null;
		for (let i = 0; i < windowClients.length; i++) {
			const windowClient = windowClients[i];
			if (windowClient.url === urlToOpenNew) {
				matchingClient = windowClient;
				break;
			}
			if (windowClient.url === urlToOpenOngoing) {
				matchingClient = windowClient;
				break;
			}
			if (windowClient.url === urlToOpenCompleted) {
				matchingClient = windowClient;
				break;
			}
			if (windowClient.url === urlToOpenProfile) {
				matchingClient = windowClient;
				break;
			}
			if (windowClient.url === urlToOpenReport) {
				matchingClient = windowClient;
				break;
			}
			if (windowClient.url === urlToOpenMenu) {
				matchingClient = windowClient;
				break;
			}
		}
		if (matchingClient) {
			return matchingClient.focus();
		} else {
			return clients.openWindow(urlToOpenNew);
		}
	});
	event.waitUntil(promiseChain);
});
