export const environment = {
	production: true,
	cookieDomain: 'demo.docstime.com', // -<< must be the domain of deployed app
	apiBaseUrl: 'https://demo.docstime.com/backend/web/index.php/v2',
	fileUrl: 'https://demo.docstime.com/backend/web/uploads',
	firebase: {
		apiKey: 'AIzaSyDhEd1QHsJM9m8yKI4AtoErPeqMKmOJG4s',
		authDomain: 'menukard-66b1e.firebaseapp.com',
		projectId: 'menukard-66b1e',
		storageBucket: 'menukard-66b1e.appspot.com',
		messagingSenderId: '964534995904',
		appId: '1:964534995904:web:3c0a90c26b69ed9a7ad625',
		measurementId: 'G-86Q6H4TXDJ',
	}
};
