import { Injectable } from '@angular/core';
import { Route } from '@angular/router';
import { Observable, of } from 'rxjs';
import { timer } from 'rxjs/internal/observable/timer';
import { mergeMap } from 'rxjs/operators';

@Injectable({
	providedIn: 'root'
})
export class CustomPreloadService {
	preload(route: Route, loadMe: () => Observable<any>): Observable<any> {
		if (route.data && route.data.preload) {
			const delay: number = route.data.delay;
			return timer(delay).pipe(
				mergeMap(_ => {
					return loadMe();
				}));
		} else {
			return of(null);
		}
	}
}
