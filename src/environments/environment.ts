// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
	production: false,
	cookieDomain: 'localhost', // -<< must be 'localhost'
	apiBaseUrl: 'https://demo.docstime.com/backend/web/index.php/v2',
	fileUrl: 'https://demo.docstime.com/backend/web/uploads',
	firebase: {
		apiKey: 'AIzaSyDhEd1QHsJM9m8yKI4AtoErPeqMKmOJG4s',
		authDomain: 'menukard-66b1e.firebaseapp.com',
		projectId: 'menukard-66b1e',
		storageBucket: 'menukard-66b1e.appspot.com',
		messagingSenderId: '964534995904',
		appId: '1:964534995904:web:3c0a90c26b69ed9a7ad625',
		measurementId: 'G-86Q6H4TXDJ',
	}
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
